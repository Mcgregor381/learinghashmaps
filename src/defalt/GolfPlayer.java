package defalt;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Andrew
 */
public class GolfPlayer 
{
   private String Name;
   private String Country;
   private int TotalPts;
   private int Events;
   private String Key;
    
         public GolfPlayer() 
          {}
         
         public GolfPlayer(String Name, String Country, int TotalPts, int Events, String Key){
             this.Key = Key;
             this.Name = Name;
             this.Country = Country;
             this.TotalPts = TotalPts;
             this.Events = Events;
           } 
    public String getName(){
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public int getTotalPts() {
        return TotalPts;
    }

    public int setTotalPts(int TotalPts) {
        this.TotalPts = TotalPts;
		return TotalPts;
    }

    public int getEvents() {
        return Events;
    }

    public String setKey() {
        return this.Key = Integer.toString(Keygen());
    }
    public String getKey() {
        return this.Key ;
    }

    public int setEvents(int Events) {
        return this.Events = Events;
    }

       @Override
       public String toString() 
       {
           String tmp1;
           String tmp2;
           String tmp3;
           String tmp4;

           tmp1 = String.format("%-24s", Name); // use 25 spaces left aligned
           tmp2 = String.format("%-5s", Country); // use 5 spaces left aligned
           tmp3 = String.format("%-6s", TotalPts);
           tmp4 = String.format("%-5s", Events);

           return "  Player Name: " + tmp1 +  "\t"  + tmp2 + "  , TotalPts :" 
                   + tmp3 + "  , Events played: " + tmp4 + "}\n";
       }  

          @Override
           public boolean equals(Object GolfPlayer) {
               if (GolfPlayer == null) {
                   return false;
               }
               if (getClass() != GolfPlayer.getClass()) {
                   return false;
               }
               final GolfPlayer other = (GolfPlayer) GolfPlayer;
               
               if (!Objects.equals(this.Name, other.Name)) {
                   return false;
               }
                if (!Objects.equals(this.Events, other.Events)) {
                   return false;
               }
            return true;
           }
          
           public int Keygen(){
              int key1 = Name.hashCode();
              int key2 = Country.hashCode();
              int keygen=key2*key1/2;
           return keygen;
           }

 }