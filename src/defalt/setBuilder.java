package defalt;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;

/**
 *Andrew Mcgregor   
 * EC****107
 */
public class setBuilder {


	  public static <T> Set<T> union(Set<T> setA, Set<T> setB){
	    Set<T> tmp = new HashSet<T>(setA);
	    tmp.addAll(setB);
	    return tmp;
	   }
  
	public static <T> Set<T> intersection(Set<T> setA, Set<T> setB){
	    Set<T> tmp = new HashSet<T>();
	    for (T x : setA){   
	      if (setB.contains(x)){
	        tmp.add(x);
	       }
	     }
	    return tmp;
	  }
	
	
	public static <T> Set<T> difference(Set<T> setA, Set<T> setB){
	    Set<T> tmp = new HashSet<T>(setA);
	    tmp.removeAll(setB);
	    return tmp;
	  }
	
	public static <T> Set<T> symDifference(Set<T> setA, Set<T> setB){
	    Set<T> tmp1,tmp2;
	    tmp1 = union(setA, setB);
	    tmp2 = intersection(setA, setB);
	    return difference(tmp1, tmp2);
	  }
	
	public static <T> boolean isSubset(Set<T> setA, Set<T> setB){
	    return setB.containsAll(setA);
	  }
	
	public static <T> boolean isSuperset(Set<T> setA, Set<T> setB){
	    return setA.containsAll(setB);
	  }
	
	    static Set<GolfPlayer> union(Map<String, GolfPlayer> cpMap1, Map<String, GolfPlayer> cpMap2) {
	        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	    }

}