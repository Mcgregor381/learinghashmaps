package defalt;


import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;

public class HashMapAssessment {

	   
	   
	 public static void main(String[] args){
	
	     fileReader ecf = new fileReader();
	     
	     Map<String,GolfPlayer> cpMap1 = new LinkedHashMap<String,GolfPlayer>(ecf.loadFile("Golf Map Data 1.txt")); 
	     Map<String,GolfPlayer> cpMap2 = new LinkedHashMap<String,GolfPlayer>(ecf.loadFile("Golf Map Data 2.txt"));
	
	       
	     Set<String> keys1 = cpMap1.keySet();
	     Set<String> keys2 = cpMap2.keySet();
	     
	     
	     System.out.println("\nHere are the keys \n");
	     
	     for(String k : keys1){
	       System.out.println("Key = "+k);  
	      }
	     
	     for(String k2 : keys2){
	       System.out.println("Key = "+k2);  
	      }
	     
	     Collection<GolfPlayer> values = cpMap1.values();
	     Collection<GolfPlayer> values2 = cpMap2.values();
	
	     System.out.println("\nHere are the values\n");
	     
	     for(GolfPlayer c : values){
	        System.out.format("%-25s",c.getName());
	        System.out.format("%-8s",c.getCountry());
	        System.out.println(c.getTotalPts()+" \t"+c.getEvents());
	      }   
	     System.out.println("\n********************\n"); 
	     
	      for(GolfPlayer d : values2)
	      {
	        System.out.format("%-25s",d.getName());
	        System.out.format("%-8s",d.getCountry());
	        System.out.println(d.getTotalPts()+" \t"+d.getEvents());
	      }   
	      
	       Set<Map.Entry<String,GolfPlayer>> keyvalue1 = cpMap1.entrySet();
	       Set<Map.Entry<String,GolfPlayer>> keyvalue2 = cpMap2.entrySet();
	      
	       System.out.println("\n****** Map 1 data ******\n"); 
	      for(Map.Entry<String,GolfPlayer> kv1 : keyvalue1)
	       {
	         System.out.println("Key = "+kv1);  
	       }
	     
	      System.out.println("\n****** Map 2 data ******\n"); 
	      for(Map.Entry<String,GolfPlayer> kv2 : keyvalue2)
	       {
	         System.out.println("Key = "+kv2);  
	       }
	      
	     System.out.println("\n********************\n"); 
	     
	        System.out.println("\nFind player with key JSUSA \n");
	        System.out.println(cpMap1.get("JSUSA"));
	        System.out.println("\nFind player with key ABCDE \n");
	        System.out.println(cpMap1.get("ABCDE"));
	     
	     Set<String> setA = new HashSet<String>();
	     Set<String> setB = new HashSet<String>();
	     
	      for(GolfPlayer c : values ){
	       setA.add(c.getName()); 
	      }   
	
	      for(GolfPlayer d : values2 ){
	       setB.add(d.getName());
	      
	      }    
	     
	     System.out.println("\n********************\n"); 
	     System.out.println("Set1 "+setA);
	     System.out.println("\n********************\n"); 
	     System.out.println("Set2 "+setB);
	     System.out.println("\n********************\n"); 
	     System.out.println("common players \n\n"+ setBuilder.intersection(setA, setB));
	   
	     System.out.println("\n********************\n"); 
	     System.out.println("difference between setA and SetB \n\n"+ setBuilder.difference(setA, setB));
	     
	     System.out.println("\n********************\n"); 
	     System.out.println("difference between setB and SetA \n\n"+ setBuilder.difference(setB, setA));
	     
	     
	     
	     
	     
	     
	     
	     
	     
	         System.out.println("==================================Part 1===========================================");
	         
	         
	         
	         
	        System.out.println("\n*********************Task 1 - Add all the players from the file to a Map***************************\n");
	        System.out.println("Map1 ");
	        Map<String,GolfPlayer> cpMap01 = 
	             new LinkedHashMap<String,GolfPlayer>(ecf.loadFile("Golf Map Data 1.txt")); 
	        System.out.println("*********************END Task 1 - Add all the players from the file to a Map***************************");
	        System.out.println("*********************Task 2 - Make a backup of the map***************************");
	         System.out.println("Back up Map1 ");
	        Map<String,GolfPlayer> backupcpMap1 = 
	             new LinkedHashMap<String,GolfPlayer>(ecf.loadFile("Golf Map Data 1.txt")); 
	         System.out.println("*********************END Task 2 - Make a backup of the map***************************");
	         System.out.println("*********************Task 3 - List the keys only***************************");
	     Set<String> keys01 = cpMap1.keySet();
	     System.out.println("\n Map 1 \n");
	     for(String k : keys01)
	      {
	       System.out.println("Key = "+k);  
	      }
	        System.out.println("\n Map 1 \n");
	     System.out.println("*********************END Task 3 - List the keys only***************************");
	     System.out.println("*********************Task 4 - List the values only***************************");
	     Collection<GolfPlayer> values01 = cpMap1.values();
	     System.out.println("\nHere are the values\n");
	     for(GolfPlayer c : values01)
	      {
	        System.out.format("%-25s",c.getName());
	        System.out.format("%-8s",c.getCountry());
	        System.out.println(c.getTotalPts()+" \t"+c.getEvents());
	      }   
	     System.out.println("\n********************\n"); 
	      System.out.println("*********************END Task 4 - List the values only***************************");
	      System.out.println("*********************Task 5 - List the key and values using Map. Entry<>***************************");
	              Set<Map.Entry<String,GolfPlayer>> keyvalue01 = cpMap1.entrySet();     
	       System.out.println("\n****** Map 1 data ******\n"); 
	      for(Map.Entry<String,GolfPlayer> kv1 : keyvalue01)
	       {
	         System.out.println("Key = "+kv1);  
	       }
	        System.out.println("*********************END Task 5 - List the key and values using Map. Entry<>***************************");
	        System.out.println("*********************Task 6 - Sum and display the total points***************************");
	        double total = 0;
	        for (GolfPlayer g : values){
	        total = total + g.getTotalPts();   
	        }
	         System.out.println(total);
	        System.out.println("*********************END Task 6 - Sum and display the total points***************************");
	        System.out.println("********************* Task 7 - Given a key find a player with that key***************************");
	        System.out.println("\nFind player with key JO52 \n");
	        System.out.println(cpMap1.get("JO52"));
	        System.out.println("********************* END Task 7 - Given a key find a player with that key***************************");
	        System.out.println("********************* Task 8 - Given a key that does not exist display relevant message***************************");
	        System.out.println("\nFind player with key ABCDE \n");
	        System.out.println(cpMap1.get("ABCDE")); 
	        System.out.println("********************* END Task 8 - Given a key that does not exist display relevant message***************************");
	        
	        System.out.println("********************* Task 9 - Try to add a player with a key that already exists***************************");
	        
	        Map<Integer,String> map=new LinkedHashMap(cpMap1);
	
	    
	        System.out.println("Adding key J052......"); 
	         System.out.println("Added key J052."); 
	        
	       
	        map.put(1,"J052");
	        
	        System.out.print(keys1);
	
	        System.out.println("********************* END Task 9 - Try to add a player with a key that already exists***************************");
	        
	        //Part 2
	          System.out.println("==================================Part 2===========================================");
	        System.out.println("\"*********************Task A Add the players from the second file to a MAP*********************");
	        
	          System.out.println("Map2 ");
	     Map<String,GolfPlayer> cpMap02 = 
	             new LinkedHashMap<String,GolfPlayer>(ecf.loadFile("Golf Map Data 2.txt"));
	     
	  
	        
	      
	     System.out.println("\n*********************END Task A Add the players from the second file to a MAP *********************\n");
	        System.out.println("\n*********************Task B Add the points in both maps and determine which group has more points.*********************\n");
	        
	        
	        
	        System.out.println("\nMap 1\n");
	        double total2 = 0;
	        for (GolfPlayer g : values)
	        {
	        total2 = total2 + g.getTotalPts();
	            
	        }
	        System.out.println(total2);
	        
	        System.out.println("\nMap 2\n");
	        double total3 = 0;
	        for (GolfPlayer h : values2){
	        	total3 = total3 + h.getTotalPts();    
	        }
	        
	        System.out.println(total3);
	        System.out.println("\n*********************END Task B Add the points in both maps and determine which group has more points.*********************\n");
	        System.out.println("***********************Task C which mapping has the most American players in it?*********************");
	        
	//        Golfer g = new Golfer();
	        
	        int nusa1=0;
	        int nusa2=0;
	        
	        for( GolfPlayer g: values01){   
	            if(g.getCountry().equals("USA"))
	            {
	                nusa1++;
	            }
	        }
	        System.out.println("There are "+nusa1+"in map 1");
	        
	        for(GolfPlayer g: values2){
	            if(g.getCountry().equals("USA")){
	                nusa2++;
	            }
	        }
	        
	         System.out.println("There are "+nusa2+"in map 2");
	         
	          int more = nusa2 - nusa1;
	        
	        System.out.println("Group 1 has " + more + " American player more  than Group 2");
	        System.out.println("***********************END Task C which mapping has the most American players in it?*********************");
	        System.out.println("***********************Task D Create a union and find who played the most events?***********************");
	        
	        int counter = 1;
	        Map<String,GolfPlayer> tempgMap1 = cpMap1; 
	        Map<String,GolfPlayer> tempgMap2 = cpMap2;
	        Set<String> Universal = new HashSet<String>();
	        Universal = setBuilder.union(keys1, keys2);
	
	        System.out.println(Universal);
	        System.out.println("\nintersection "+ setBuilder.intersection(keys1, keys2));
	        
	        cpMap1.putAll(tempgMap1);
	        cpMap2.putAll(tempgMap2);
	        
	        
	        Set<Map.Entry<String,GolfPlayer>> keyvalue3 = cpMap1.entrySet();
	        System.out.println("\n****** Map 3 data ******\n");
	        
	        for(Map.Entry<String,GolfPlayer> kv3 : keyvalue3)
	        {
	           System.out.println(counter + " = " +kv3);
	           counter++;
	        }
	        
	        Collection<GolfPlayer> values3 = cpMap2.values();
	        String golferkey = null;
	        counter = 0;
	        for (GolfPlayer h : values3){
	            if(h.getEvents() > counter){
	                 counter = h.getEvents();
	                 //golferkey = h.setKey();
	            }
	        }
	        
	        System.out.println("\n\nThe most events played is " + counter + " by " + cpMap2.get(golferkey));
	        System.out.println("***********************END Task D Create a union and find who played the most events?***********************");
	        System.out.println("***********************Task E Who played the least events.***********************");
	        counter = 100; 
	       for (GolfPlayer j : values3){
	           if(j.getEvents() < counter){
	               counter = j.getEvents();
	               golferkey = j.setKey();
	           }
	       }
	       
	       
	       System.out.println("\n\nThe least events played is " + counter + " by " + cpMap2.get(golferkey));
	       System.out.println("***********************END Task E Who played the least events.***********************");
	       
	        System.out.println("Task F How many different players are there in total?"+ "\n" 
	        					+"\n\nThe number of players over the 2 years are " + cpMap2.size());
	        counter = 100; 
	       for (GolfPlayer j : values3){
	           if(j.getEvents() < counter){
	               counter = j.getEvents();
	               golferkey = j.setKey();
	           }
	       }
	       
	       System.out.println("\n\nThe least events played is " + counter + " by " + cpMap2.get(golferkey));
	       System.out.println("\n\nThe number of players over the 2 years are " + cpMap2.size());
	       
	       System.out.println("Task G Create a Mapping of non American players.");
	       
	       Map<String,GolfPlayer> gMap4 = new LinkedHashMap<String,GolfPlayer>();
	       
	       
	       for(GolfPlayer g : values3){
	           if(!g.getCountry().equalsIgnoreCase("USA")){
	               gMap4.put(g.setKey(), g);
	           }
	       }
	       
	        System.out.println("***********END Task G Create a Mapping of non American players.***********");
	        System.out.println("Task H List the keys only for non American players");
	       Set<String> keys4 = gMap4.keySet();
	       
	       System.out.println("\n\n *********Keys for none American players************");
	       for(String k : keys4)
	       {
	           System.out.println("Key = "+k);  
	       }
	       System.out.println("********************END Task H List the keys only for non American players*******************");
	       System.out.println("********************Task I List the values only for non American players***********************");
	       System.out.println("\n\n **************Values for none American Players ************");
	       
	       Collection<GolfPlayer> values4 = gMap4.values();
	       
	       for(GolfPlayer g : values4){
	           System.out.println("Values = " + g);
	       }
	       
	        System.out.println("******************END Task I List the values only for non American players**********************");
	        System.out.println("******************Task J List the key and values using Map.Entry<> for non Americans************");
	       
	       Set<Map.Entry<String,GolfPlayer>> keyvalue4 = gMap4.entrySet();
	       System.out.println("\n****** Non American data ******\n");
	       counter = 1;
	       
	       for(Map.Entry<String,GolfPlayer> kv4 : keyvalue4){
	           System.out.println(counter + " = " +kv4);
	           counter++;
	       }
	       System.out.println("******************END Task J List the key and values using Map.Entry<> for non Americans************");
	       System.out.println("*****************Task K Sum and display the total points for non American players******************");
	        double total1 = 0;
	        
	        total1 = 0;
	        for(GolfPlayer f : values4){            
	                total1 = total1 + f.getTotalPts();
	        }
	         
	        System.out.println("\n\n *********** Total points for non Americans **********");
	        System.out.println("\n\n ==== " + total1);
	         System.out.println("*****************END Task K Sum and display the total points for non American players******************");
	            System.out.println("**************Task L Remove all players with less than 45 events played.******************");
	        System.out.println("\n\n**********Players with 45 or more events*******");
	        for (GolfPlayer g : values3)
	        {   
	            if(g.getEvents() < 45)
	            { 
	                cpMap2.remove(g);
	            }
	        }
	        System.out.println("**************END Task L Remove all players with less than 45 events played.******************");
	        System.out.println("***************Task M Empty both maps and show they are empty***********************");
	        for(GolfPlayer g : values3)
	        {
	            System.out.println(g + " events == " + g.getEvents());
	            counter++;
	        }
	        cpMap1.clear();
	        cpMap2.clear();
	      
	        System.out.println("Map1 contains " + cpMap1);
	        System.out.println("Map2 contains " + cpMap2);
	        
	        System.out.println("***************END Task M Empty both maps and show they are empty***********************");
	               
	    }   
}